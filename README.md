# Sam Chat

A little project to create a chat application

## Purpose

I find it very interesting how much we, the US, talk about chat platforms being iMessage for iOS and anything else for all other platforms but that SMS is the default layer for it all. While there are thousand of solutions in the chatting space, my engineering brain is quite intriguied if it would be possible to build a chat platform.

This by no means that this platform will be commericalized or realised to the public in any way. In all tense and purposes this should be seen as a proof of concept/passion project that _if_ there is a want to have a public release then it can be used as the starting grounds to make sure it is production ready.

## Requirements

- [ ] End-2-End Encryption
- [ ] Encryptoin in Transit
- [ ] Interopable
- [ ] Cross Platform

## Technologies

As this is a personal project, the following technologies _could_ change if I get bored or deemed fit to change to something else. BUT the goal is to learn about the wolrd of chat applications and what it takes to build one that meets the requirements above

### Infrastructure

### Backend

### Frontends

#### Mobile - iOS & Android

#### Desktop

#### Web